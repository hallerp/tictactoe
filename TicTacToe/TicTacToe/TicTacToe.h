#pragma once
#include <iostream>


class TicTacToe
{
private:

	char m_board[10];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;
	bool m_validMove;

public:
	TicTacToe()
	{
		for (int i = 1; i <= 9; i++)
		{
			char a = '0' + i;
			SetBoard(a, i);
		}
		SetPlayerTurn1('O');
		SetWinner(' ');
		SetNumTurns(0);
		SetValidMove(true);
	}

	virtual ~TicTacToe() { };


	char GetWinner() const { return m_winner; }
	void SetWinner(const char winner) { m_winner = winner; }	
	
	bool GetValidMove() const { return m_validMove; }
	void SetValidMove(const bool move) { m_validMove = move; }

	char GetBoard(int i) const { return m_board[i]; }
	void SetBoard(char board, int n) { m_board[n] = board; }

	char GetPlayerTurn1() const { return m_playerTurn; }
	void SetPlayerTurn1(const char turn) { m_playerTurn = turn; }

	int GetNumTurns() const { return m_numTurns; }
	void SetNumTurns(const int numTurns) { m_numTurns = numTurns; }

	void DisplayBoard()
	{
		std::cout << "\n " << m_board[1] << " | " << m_board[2] << " | " << m_board[3] << " \n";
		std::cout << "-----------\n";
		std::cout << " " << m_board[4] << " | " << m_board[5] << " | " << m_board[6] << " \n";
		std::cout << "-----------\n";
		std::cout << " " << m_board[7] << " | " << m_board[8] << " | " << m_board[9] << " \n";
	}

	bool IsOver()
	{
		SetNumTurns(GetNumTurns() + 1);

		if (m_board[1] == m_board[2] && m_board[1] == m_board[3])
		{
			SetWinner(m_board[1]);
			return true;
		}
		else if (m_board[1] == m_board[5] && m_board[1] == m_board[9])
		{
			SetWinner(m_board[1]);
			return true;
		}
		else if (m_board[1] == m_board[4] && m_board[1] == m_board[7])
		{
			SetWinner(m_board[1]);
			return true;
		}
		else if (m_board[2] == m_board[5] && m_board[2] == m_board[8])
		{
			SetWinner(m_board[2]);
			return true;
		}
		else if (m_board[3] == m_board[6] && m_board[3] == m_board[9])
		{
			SetWinner(m_board[3]);
			return true;
		}
		else if (m_board[3] == m_board[5] && m_board[3] == m_board[7])
		{
			SetWinner(m_board[3]);
			return true;
		}
		else if (m_board[4] == m_board[5] && m_board[4] == m_board[6])
		{
			SetWinner(m_board[4]);
			return true;
		}
		else if (m_board[7] == m_board[8] && m_board[7] == m_board[9])
		{
			SetWinner(m_board[7]);
			return true;
		}
		else if (GetNumTurns() == 10)
			return true;
		else
			return false;
	}


	char GetPlayerTurn()
	{
		if (GetPlayerTurn1() == 'X' && m_validMove == true)
		{
			SetPlayerTurn1('O');
		}
		else if (GetPlayerTurn1() == 'O' && m_validMove == true)
		{
			SetPlayerTurn1('X');
		}
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		if (position == 1 && m_board[1] == '1')
			return true;
		else if (position == 2 && m_board[2] == '2')
			return true;
		else if (position == 3 && m_board[3] == '3')
			return true;
		else if (position == 4 && m_board[4] == '4')
			return true;
		else if (position == 5 && m_board[5] == '5')
			return true;
		else if (position == 6 && m_board[6] == '6')
			return true;
		else if (position == 7 && m_board[7] == '7')
			return true;
		else if (position == 8 && m_board[8] == '8')
			return true;
		else if (position == 9 && m_board[9] == '9')
			return true;
		else
		{
			SetValidMove(false);
			return false;
		}

	}

	void Move(int position)
	{
		SetBoard(GetPlayerTurn1(), position);
	}
	
	void DisplayResult()
	{
		if (GetWinner() == ' ')
		{
			std::cout << "The game is a Tie!\n\n";
		}
		else
		{
			std::cout << "Player " << GetWinner() << " is the winner!\n\n";
		}
	}
};